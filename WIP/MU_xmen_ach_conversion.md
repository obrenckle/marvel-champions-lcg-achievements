## PHOENIX FIVE:

- Win the Phoenix Five campaign.
- Win the Phoenix Five campaign with Moderate Challenge.
- Win the Phoenix Five campaign with Hard Challenge.
- Win the Phoenix Five campaign with Heroic Challenge.
- Defeat Namor as the final Phoenix Five Villain.
- Defeat Magik as the final Phoenix Five Villain.
- Defeat Colossus as the final Phoenix Five Villain.
- Defeat Emma Frost as the final Phoenix Five Villain.
- Defeat Cyclops as the final Phoenix Five Villain.

# Hero Feats

## CORE BOX:

- Defeat Magneto with Professor X and any other original X-Men team.
- Mind Readers: Defeat a Villain with Professor X, Jean Grey, and Emma Frost.

## BLUE TEAM / GOLD TEAM:

- Win playing the Heroes from the Blue Team box over the Heroes from the Gold Team box.
- Win playing the Heroes from the Gold Team box over the Heroes from the Blue Team box.
- Schism: Win a Team vs Team game with Cyclops while Wolverine is on the other team.
- Schism Rematch: Win a Team vs Team game with Wolverine while Cyclops is on the other team.

## DEADPOOL:

- Defeat a Villain using the Deadpool Challenge.
- Besties: Win with Deadpool and Bob in your team.

## FANTASTIC FOUR:

- Defeat a Villain using the Takeover Challenge.
- Defeat a Villain using the Aggressive Takeover Challenge.
- Teamwork: Defeat Doctor Doom with only members of the Fantastic Four.
- Unlikely Allies: Win with Mister Fantastic and Doctor Doom in your team.
- Fantastic Family Sinergy: Win playing at least 4 Teamwork cards in the Storyline.
- Self-limitations: Win with Silver Surfer in your team without playing Cosmic Awareness.

## FIRST CLASS:

- Win without using the Danger Room.
- Win using the Danger Room at least 6 times.
- Win after emptying the Training deck.

## DAYS OF FUTURE PAST:

- Defeat a Villain using the Sentinel I Challenge.
- Defeat a Villain using the Sentinel II Challenge.
- Defeat a Villain using the Sentinel III Challenge.

## PHOENIX FIVE:

- Win the Phoenix 5 campaign without Hope Summers in your team.

## X-FORCE:

- Defeat a Villain using the Hazardous Locations Challenge.

## THE HORSEMEN OF APOCALYPSE:

- Defeat a Villain without using the Starlight Citadel effect.

## TEAM-UPS:

- Jumpstart: Win using only Heroes with a Starting Hand card.
- Multiplicity: Win a 2-Hero game using two different versions of the same character.
- There is only one Wolverine: Win with only versions of Wolverine in your team.
- Wagner Family: With with Mystique and Nightcrawler in your team.
- Les Liaisons Dangereuses III: Win with Mister Fantastic, Invisible Woman, and Namor in your team
- Cloak & Dagger: Win a 2-Hero game with Cloak and Dagger.
- Strong Bond: Win with Cloak and Dagger in your team and both Bond cards in the Storyline.
- Pooling Together: Win with Deadpool, Lady Deadpool, and Gwenpool in your team.
- Just like Mother and Daughter: Win with Mystique and Rogue in your team.
- Banding Together: Win with Dazzler and Strong Guy in your team.
- All the Dupes: Win with Multiple Man placing at least 1 Dupe token in each Location.
- Multiple Personality: Win with Legion playing at least 5 of his special abilities.
- Mutant Town Investigations: Win with Strong Guy and Multiple Man in your team.
- Phoenix Cry: Win with Phoenix delivering the final blow with the Phoenix Cry special effect.
- Twin Heart: Win with Marrow having used her Dual Hearts special effect.
- Native Alliance: Win with Forge, Warpath, and Mirage in your team.
- Weirdest Team-up Ever: Win a 2-Hero game with Doop and Wolverine.
- Beast Pack: Win with Beast, Feral, and Wolfsbane in your team.
- Redemption Path: Win with Warpath, Sunfire, Mystique and Rogue as your team.
- A Stunning Team: Win with Iceman, Psylocke, Banshee, and Pixie as your team, all using a Stunned token at some point.
- Magnetic Personalities: Win with Magneto and Polaris in your team.
- Burn, Baby, Burn: Win with Sunfire, Firestar, and Boom Boom in your team.
- Anti-Heroes United: Win Apocalypse, Doctor Doom, Magneto, and Namor as your team.
- A Tale of Ice, Fire, and Amazing Friendship: Win a 3-Hero game with Firestar, Iceman, and a third Hero of your choice.
- Xavier’s School for Gifted Youngsters: Win with Professor X in your team, using the Danger Room Challenge.
- Xavier Institute for Higher Learning: Win with Cyclops and Emma Frost in your team, using the Danger Room Challenge.
- Alternate Headmaster: Win with Magneto in your team, using the Danger Room Challenge.
- New Mutants: Win with only New Mutants members in your team (Cannonball, Mirage, Wolfsbane, Sunspot, Warlock, Magik, or Feral)
- Alpha Strike: Win game with only Alpha Flight members in your team.
- Alpha Teamplay: Win with only Alpha Flight members in your team and having played at least 3 Alpha Team cards.
- Excalibur: Win with Captain Britain, Nightcrawler, Kitty Pride, and Phoenix as your team.
- Exiled: Win with Blink, Beast, Forge, and Polaris as your team.

## SHOWDOWNS:
