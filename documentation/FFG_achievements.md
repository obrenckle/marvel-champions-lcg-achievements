# Iron Man

_I am Iron Man_: Finish a game with all 7 pieces of Iron Man's signature Tech upgrade cards in play (Arc Reactor, Mark V Armor, Mark V Helmet, Powered Gauntletsx2, Rocket Boots x2).

_Heart of Iron_: Use Iron Man's basic Thwart power to remove 10 threat or more in a single turn.

_Stark Legacy_: Use Stark Tower to return all 7 different Iron Man's signature Tech upgrade cards to your hand in a single game.

# Black Panther

_Strength of Wakanda_: Trigger each of Black Panther's signature upgrade cards as the final step in a Wakanda Forever! sequence in a single game.

_Hail to the King!_: Using a single copy of Wakanda forever, defeat 3 enemies and remove the last threat from a scheme in play.

_Why Are You Hitting Yourself?_: Do the final damage to the villain to win the game using your Retaliate Hero Power.

# Captain Marvel

_Warbird_: Play all 3 copies of Photonic Blast in a single turn.

_Higher. Further. Faster._ Draw 30 cards outside of the draw phase in a single game.

_Power Overwhelming_: Use both copies of Energy Channel to deal 20 damage in a single turn.

# Spider-Man

_Can he swing, from a thread..._: Play all 3 Spinning Web Kicks in a single turn.

_Friendly Neighborhood Spide-Man_: Win a game in which the villain never damages any hero

_Steal the Show_: Deal 16 damage or more with Black Cat in a single turn.

# She-Hulk

_Now I'm Really Angry!_ Deal 14 or more damage to the villain using Gamma Slam.

_Ace Attorney_: Use both copies of Legal Practice to remove 10 threat or more in a single turn.

_Seven-Eight-Nine-Ten Punch_: Deal 30 damage or more in a single turn using only your basic attack power.
