# Basic Achievements

- Win a game in Xavier Solo Mode.
- Win a game with an Anti-Hero as a Hero.
- Win a game using only Anti-Heroes as Heroes.
- Win a game with 2 Players.
- Win a game with 3 Players.
- Win a game with 4 Players.
- Complete all Mission cards.
- Complete all Mission cards with Moderate Challenge.
- Complete all Mission cards with Hard Challenge.
- Complete all Mission cards with Heroic Challenge.
- Win without any Hero being KO’d.
- Win without any Hero being KO’d with Moderate Challenge.
- Win without any Hero being KO’d with Hard Challenge.
- Win without any Hero being KO’d with Heroic Challenge.
- Win without the Villain ever triggering an Overflow.
- Win before the 6th Master Plan card is played.
- Win without using any Special Effect cards.
- Win without any Hero taking damage.
- Win without using any Action tokens.

# Super Villain Feats

- Defeat the Super Villain with 2 Heroes.
- Defeat the Super Villain with 3 Heroes.
- Defeat the Super Villain with 4 Heroes.
- Defeat the Super Villain without using any Super Hero card.
- Defeat the Super Villain without using any Action tokens.
- Play all Super Hero cards on a single Villain turn (with 2 Heroes).
- Play all Super Hero cards on a single Villain turn (with 3 Heroes).
- Play all Super Hero cards on a single Villain turn (with 4 Heroes).
- Win as the Super Villain without using any Super Villain card.
- Win as the Super Villain without taking a single damage.
- Win as the Super Villain before the Heroes solve any Mission.
- Win as the Super Villain with a single Health left.
- Win as the Super Villain by fulfilling your Villainous Plot.
- Win as the Super Villain by running out of Master Plan cards.

# Team vs Team Feats

- Defeat the Villain using the Accelerated Villain Challenge.
- Your team wins without the other team dealing a single damage to the Villain.
- Your team wins delivering the final blow to the Villain.

# Villain Fights

## CORE BOX:

- Defeat Magneto with Hard Challenge.
- Defeat Magneto with Heroic Challenge.
- Defeat Magneto withoutcompleting the Cerebro Mission.
- Defeat Mystique with Hard Challenge.
- Defeat Mystique with Heroic Challenge.
- Defeat Mystique without her ever triggering an Overflow.
- Defeat Sabretooth with Hard Challenge.
- Defeat Sabretooth with Heroic Challenge.
- Defeat Sabretooth without any Hero being KO’d.
- Defeat Juggernaut with Hard Challenge.
- Defeat Juggernaut with Heroic Challenge.
- Defeat Juggernaut without completing the Clear Threats Mission.

## BLUE TEAM:

- Defeat Mister Sinister with Hard Challenge.
- Defeat Mister Sinister with Heroic Challenge.
- Defeat Mister Sinister before he takes more than 5 DNA Samples.
- Defeat Mister Sinister without any hero losing all their DNA Samples.

## GOLD TEAM:

- Defeat Sebastian Shaw with Hard Challenge.
- Defeat Sebastian Shaw with Heroic Challenge.
- Defeat Sebastian Shaw with both Black Queen and White Queen still in play.

## DEADPOOL:

- Score 15 or more points against Deadpool.
- Score 30 or more points against Deadpool.
- Score 50 or more points against Deadpool.
- Defeat Bob with Hard Challenge.
- Defeat Bob with Heroic Challenge.
- Defeat Bob with the Retirement track on 6 or less.
- Defeat Bob with the Retirement track on 10 or less.
- Defeat Bob with the Retirement track on 11.

## FANTASTIC FOUR:

- Defeat Doctor Doom with Hard Challenge.
- Defeat Doctor Doom with Heroic Challenge.
- Defeat Doctor Doom with the Doom track on 19.
- Defeat Super-Skrull with Hard Challenge.
- Defeat Super-Skrull with Heroic Challenge.
- Defeat Super-Skrull without completing the Clear Threats Mission.

## FIRST CLASS:

- Defeat Scarlet Witch & Quicksilver with Hard Challenge.
- Defeat Scarlet Witch & Quicksilver with Heroic Challenge.
- Defeat both Scarlet Witch & Quicksilver on the same Hero turn.
- Defeat Scarlet Witch & Quicksilver with a single Reality Warping in the Storyline.
- Defeat Scarlet Witch & Quicksilver with both Reality Warping in the Storyline.

## DAYS OF FUTURE PAST:

- Defeat Nimrod with Hard Challenge.
- Defeat Nimrod with Heroic Challenge.
- Defeat Nimrod with Nimrod Activation track in the white zone.
- Defeat Nimrod with Nimrod Activation track in the yellow zone.
- Defeat Nimrod with Nimrod Activation track in the red zone.

## PHOENIX FIVE:

- Win the Phoenix Five campaign.
- Win the Phoenix Five campaign with Moderate Challenge.
- Win the Phoenix Five campaign with Hard Challenge.
- Win the Phoenix Five campaign with Heroic Challenge.
- Defeat Namor as the final Phoenix Five Villain.
- Defeat Magik as the final Phoenix Five Villain.
- Defeat Colossus as the final Phoenix Five Villain.
- Defeat Emma Frost as the final Phoenix Five Villain.
- Defeat Cyclops as the final Phoenix Five Villain.

## X-FORCE:

- Defeat Stryfe with Hard Challenge.
- Defeat Stryfe with Heroic Challenge.
- Defeat Stryfe without completing the Defeat Thugs Mission.

## THE HORSEMEN OF APOCALYPSE:

- Defeat Apocalypse with Hard Challenge.
- Defeat Apocalypse with Heroic Challenge.
- Defeat Apocalypse after defeating all four Horsemen in the prelude.
- Defeat Apocalypse with no Horsemen left in play.
- Defeat Apocalypse with all Horsemen in play.
- Defeat Apocalypse without playing the prelude first.
- Defeat Apocalypse with the Apocalypse tracker on 15.

# KICKSTARTER promos

- Defeat Arcade with Hard Challenge.
- Defeat Arcade with Heroic Challenge.
- Defeat Arcade without any Hero being KO’d.
- Defeat Avalanche with Hard Challenge.
- Defeat Avalanche with Heroic Challenge.
- Defeat Avalanche with 6 or less faceup Master Plan cards in the Storyline.
- Defeat Brood Queen with Hard Challenge.
- Defeat Brood Queen with Heroic Challenge.
- Defeat Brood Queen with no Brood in play.
- Defeat Brood Queen with 5 Brood in play.
- Defeat Callisto with Hard Challenge.
- Defeat Callisto with Heroic Challenge.
- Defeat Callisto without defeating any Henchmen.
- Defeat Dark Phoenix with Hard Challenge.
- Defeat Dark Phoenix with Heroic Challenge.
- Defeat Dark Phoenix with no Locations facedown.
- Defeat Deathbird with Hard Challenge.
- Defeat Deathbird with Heroic Challenge.
- Defeat Deathbird with Vulcan still in play.
- Defeat Emma Frost with Hard Challenge.
- Defeat Emma Frost with Heroic Challenge.
- Defeat Emma Frost with no Henchmen still in play.
- Defeat Lady Deathstrike with Hard Challenge.
- Defeat Lady Deathstrike with Heroic Challenge.
- Defeat Lady Deathstrike with the Hunted Hero delivering the final blow.
- Defeat Legion with Hard Challenge.
- Defeat Legion with Heroic Challenge.
- Defeat Legion with both Endgame cards in the Storyline.
- Defeat Marrow with Hard Challenge.
- Defeat Marrow with Heroic Challenge.
- Defeat Marrow with both Dual Heart cards facedown in the Storyline.
- Defeat Mastermind with Hard Challenge.
- Defeat Mastermind with Heroic Challenge.
- Defeat Mastermind without using effects that affect the Master Plan deck.
- Defeat Mojo with Hard Challenge.
- Defeat Mojo with Heroic Challenge.
- Defeat Mojo with Spiral still in play.
- Defeat Mojo with the Ratings track in the white zone.
- Defeat Mojo with the Ratings track in the red zone.
- Defeat Namor with Hard Challenge.
- Defeat Namor with Heroic Challenge.
- Defeat Namor with all 6 Imperius Rex cards in the Storyline.
- Defeat Omega Red with Hard Challenge.
- Defeat Omega Red with Heroic Challenge.
- Defeat Omega Red with all Heroes having Crisis tokens.
- Defeat Onslaught with Hard Challenge.
- Defeat Onslaught with Heroic Challenge.
- Defeat Onslaught with his Master Plan deck depleted.
- Defeat Sauron with Hard Challenge.
- Defeat Sauron with Heroic Challenge.
- Defeat Sauron with no Crisis tokens on his Dashboard.
- Defeat Shadow King with Hard Challenge.
- Defeat Shadow King with Heroic Challenge.
- Defeat Shadow King without Heroes ever damaging each other.
- Defeat Silver Samurai with Hard Challenge.
- Defeat Silver Samurai with Heroic Challenge.
- Defeat Silver Samurai after facing all 4 Tachyon Field Sword Duels without losing any.
- Defeat Spiral with Hard Challenge.
- Defeat Spiral with Heroic Challenge.
- Defeat Spiral without any Hero being KO’d.
- Defeat Toad + Blob + Pyro with Hard Challenge.
- Defeat Toad + Blob + Pyro with Heroic Challenge.
- Defeat Toad + Blob + Pyro leaving Toad for last.
- Defeat Toad + Blob + Pyro leaving Blob for last.
- Defeat Toad + Blob + Pyro leaving Pyro for last.

# Hero Feats

## CORE BOX:

- Defeat Magneto with Professor X and any other original X-Men team.
- Stepbrothers: Defeat Juggernaut with Professor X in your team.
- Mind Readers: Defeat a Villain with Professor X, Jean Grey, and Emma Frost.

## BLUE TEAM / GOLD TEAM:

- Win playing the Heroes from the Blue Team box over the Heroes from the Gold Team box.
- Win playing the Heroes from the Gold Team box over the Heroes from the Blue Team box.
- Schism: Win a Team vs Team game with Cyclops while Wolverine is on the other team.
- Schism Rematch: Win a Team vs Team game with Wolverine while Cyclops is on the other team.

## DEADPOOL:

- Defeat a Villain using the Deadpool Challenge.
- Besties: Win with Deadpool and Bob in your team.

## FANTASTIC FOUR:

- Defeat a Villain using the Takeover Challenge.
- Defeat a Villain using the Aggressive Takeover Challenge.
- Teamwork: Defeat Doctor Doom with only members of the Fantastic Four.
- Unlikely Allies: Win with Mister Fantastic and Doctor Doom in your team.
- Fantastic Family Sinergy: Win playing at least 4 Teamwork cards in the Storyline.
- Self-limitations: Win with Silver Surfer in your team without playing Cosmic Awareness.

## FIRST CLASS:

- Win without using the Danger Room.
- Win using the Danger Room at least 6 times.
- Win after emptying the Training deck.

## DAYS OF FUTURE PAST:

- Defeat a Villain using the Sentinel I Challenge.
- Defeat a Villain using the Sentinel II Challenge.
- Defeat a Villain using the Sentinel III Challenge.

## PHOENIX FIVE:

- Win the Phoenix 5 campaign without Hope Summers in your team.

## X-FORCE:

- Defeat a Villain using the Hazardous Locations Challenge.

## THE HORSEMEN OF APOCALYPSE:

- Defeat a Villain without using the Starlight Citadel effect.

## TEAM-UPS:

- Win using only members of the original X-Men team.
- Jumpstart: Win using only Heroes with a Starting Hand card.
- Multiplicity: Win a 2-Hero game using two different versions of the same character.
- There is only one Wolverine: Win with only versions of Wolverine in your team.
- Fastball Special: Win with Colossus an Wolverine in your team, delivering the final blow with a Fastball Special using Wolverine.
- Summers Family: Win with Cyclops, Havok, Jean Grey, and Hope Summers as your team.
- Wagner Family: With with Mystique and Nightcrawler in your team.
- Les Liaisons Dangereuses: Win with Jean Grey, Cyclops, and Wolverine in your team.
- Les Liaisons Dangereuses II: Win with Magneto, Rogue, and Gambit in your team.
- Les Liaisons Dangereuses III: Win with Mister Fantastic, Invisible Woman, and Namor in your team.
- Ultimate Danger: Win with Cable, Domino, and Deadpool in your team.
- Cloak & Dagger: Win a 2-Hero game with Cloak and Dagger.
- Strong Bond: Win with Cloak and Dagger in your team and both Bond cards in the Storyline.
- Ultimate Team-up: Win with Deadpool and Cable in your team.
- Pooling Together: Win with Deadpool, Lady Deadpool, and Gwenpool in your team.
- Brothers in Arms: Win with Cyclops and Havok in your team.
- Opposites Attract: Win with Havok and Polaris in your team.
- Brother and Sister: Win with Colossus and Magik in your team.
- Just like Mother and Daughter: Win with Mystique and Rogue in your team.
- This Love is a Blast: Win with Boom Boom and Cannonball in your team.
- Banding Together: Win with Dazzler and Strong Guy in your team.
- Show Must Go On: Win with Jubilee and Dazzler in your team.
- All the Dupes: Win with Multiple Man placing at least 1 Dupe token in each Location.
- Multiple Personality: Win with Legion playing at least 5 of his special abilities.
- Mutant Town Investigations: Win with Strong Guy and Multiple Man in your team.
- Phoenix Cry: Win with Phoenix delivering the final blow with the Phoenix Cry special effect.
- Twin Heart: Win with Marrow having used her Dual Hearts special effect.
- Native Alliance: Win with Forge, Warpath, and Mirage in your team.
- Weirdest Team-up Ever: Win a 2-Hero game with Doop and Wolverine.
- Beast Pack: Win with Beast, Feral, and Wolfsbane in your team.
- Redemption Path: Win with Warpath, Sunfire, Mystique and Rogue as your team.
- A Stunning Team: Win with Iceman, Psylocke, Banshee, and Pixie as your team, all using a Stunned token at some point.
- Magnetic Personalities: Win with Magneto and Polaris in your team.
- Burn, Baby, Burn: Win with Sunfire, Firestar, and Boom Boom in your team.
- Anti-Heroes United: Win Apocalypse, Doctor Doom, Magneto, and Namor as your team.
- A Tale of Ice, Fire, and Amazing Friendship: Win a 3-Hero game with Firestar, Iceman, and a third Hero of your choice.
- Xavier’s School for Gifted Youngsters: Win with Professor X in your team, using the Danger Room Challenge.
- Xavier Institute for Higher Learning: Win with Cyclops and Emma Frost in your team, using the Danger Room Challenge.
- Jean Grey School for Higher Learning: Win with Wolverine and Kitty Pride in your team, using the Danger Room Challenge.
- Alternate Headmaster: Win with Magneto in your team, using the Danger Room Challenge.
- The Apocalypse Solution: Win with Psylocke, Archangel, Fantomex, and Deadpool as your team.
- New Mutants: Win with only New Mutants members in your team (Cannonball, Mirage, Wolfsbane, Sunspot, Warlock, Magik, or Feral)
- X-treme Team: Win with Storm, Psylocke, and Warpath in your team.
- Alpha Strike: Win game with only Alpha Flight members in your team.
- Alpha Teamplay: Win with only Alpha Flight members in your team and having played at least 3 Alpha Team cards.
- Excalibur: Win with Captain Britain, Nightcrawler, Kitty Pride, and Phoenix as your team.
- Exiled: Win with Blink, Beast, Forge, and Polaris as your team.

## SHOWDOWNS:

- Where walks the Juggernaut: Defeat Juggernaut with Human Torch in your team.
- Clones War: Defeat Stryfe with Cable in your team.
- All in the Family: Defeat Mister Sinister with Jean Grey, Cyclops, and Cable in your team.
- Rebels: Defeat Mojo with Spiral, Longshot, and Dazzler in your team.
- The Wizard of X: Defeat Mojo with Cyclops, Rogue, Wolverine, and Dazzler as your team.
- Biting the Hand: Defeat Callisto with Marrow in your team.
- Clashing Claws: Defeat Lady Deathstrike with Wolverine in your team.
- Going Mental: Defeat Shadow King with Professor X, Magneto, and Emma Frost in your team.
- Thief’s Gambit: Defeat Shadow King with Gambit in your team.
- Anarchy in the UK: Defeat Shadow King with Psylocke and Captain Britain in your team.
- Live on TV: Defeat Spiral with Longshot, Shatterstar, Psylocke, and Dazzler as your team.
- Back in the USSR: Defeat Omega Red with Colossus and Magik in your team.
- The Red and the Blue: Defeat Omega Red with Rogue, Gambit, Psylocke, and Jubilee as your team.
- Healing Factor: Defeat the Brood Queen with Wolverine and Deadpool in your team.
- Lovers’ Quarrels: Defeat Deathbird with Bishop in your team.
- Fight for Leadership: Defeat Callisto with Storm in your team.
- Phoenix Fight: Defeat Dark Phoenix with Phoenix and Jean Gray in your team.
- The Sub-Mariner: Defeat Namor with only members of the Fantastic Four in your team.
- Father and Son: Defeat Legion with Professor X in your team
