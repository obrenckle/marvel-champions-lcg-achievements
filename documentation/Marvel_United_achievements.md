# Basic Achievements

- Win a game in S.H.I.E.L.D. Solo Mode.
- Win a game with 2 Players.
- Win a game with 3 Players.
- Win a game with 4 Players.
- Complete all Mission cards.
- Complete all Mission cards with Moderate Challenge.
- Complete all Mission cards with Hard Challenge.
- Complete all Mission cards with Heroic Challenge.
- Win without any Hero being KO’d.
- Win without any Hero being KO’d with Moderate Challenge.
- Win without any Hero being KO’d with Hard Challenge.
- Win without any Hero being KO’d with Heroic Challenge.

# Challenge Feats

- Defeat a Villain using the Endangered Locations Challenge.
- Defeat a Villain using the Secret Identity Challenge.
- Defeat a Villain using the Traitor Challenge with 3 Players.
- Defeat a Villain using the Traitor Challenge with 4 Players.
- Defeat a Villain using the Plan B Challenge.
- Defeat a Villain using 2 Challenges combined.
- Defeat a Villain using 3 Challenges combined.
- Defeat a Villain using 4 Challenges combined.

# Legendary Feats

- Have no Civilians or Thugs in any Locations when the Villain is defeated.
- Defeat a Villain before their 6th Master Plan card is played.
- Defeat a Villain without using any Special Effect cards.
- Defeat a Villain without any Hero taking damage.
- Defeat a Villain with Endangered Locations, Secret Identity, Traitor, Plan B, and Heroic Challenges combined.

# Villain Fights

## CORE BOX:

- Defeat Red Skull.
- Defeat Red Skull with Moderate Challenge.
- Defeat Red Skull with Hard Challenge.
- Defeat Red Skull with Heroic Challenge.
- Defeat Ultron.
- Defeat Ultron with Moderate Challenge.
- Defeat Ultron with Hard Challenge.
- Defeat Ultron with Heroic Challenge.
- Defeat Taskmaster.
- Defeat Taskmaster with Moderate Challenge.
- Defeat Taskmaster with Hard Challenge.
- Defeat Taskmaster with Heroic Challenge.

## RISE OF THE BLACK PANTHER:

- Defeat Killmonger.
- Defeat Killmonger with Moderate Challenge.
- Defeat Killmonger with Hard Challenge.
- Defeat Killmonger with Heroic Challenge.

## ENTER THE SPIDER-VERSE:

- Defeat Green Goblin.
- Defeat Green Goblin with Moderate Challenge.
- Defeat Green Goblin with Hard Challenge.
- Defeat Green Goblin with Heroic Challenge.

## TALES OF ASGARD:

- Defeat Loki.
- Defeat Loki with Moderate Challenge.
- Defeat Loki with Hard Challenge.
- Defeat Loki with Heroic Challenge.

## GUARDIANS OF THE GALAXY REMIX:

- Defeat Ronan.
- Defeat Ronan with Moderate Challenge.
- Defeat Ronan with Hard Challenge.
- Defeat Ronan with Heroic Challenge.

# KICKSTARTER EXPANSIONS

## THE INFINITY GAUNTLET:

- Defeat Black Dwarf.
- Defeat Black Dwarf with Moderate Challenge.
- Defeat Black Dwarf with Hard Challenge.
- Defeat Black Dwarf with Heroic Challenge.
- Defeat Ebony Maw.
- Defeat Ebony Maw with Moderate Challenge.
- Defeat Ebony Maw with Hard Challenge.
- Defeat Ebony Maw with Heroic Challenge.
- Defeat Proxima Midnight.
- Defeat Proxima Midnight with Moderate Challenge.
- Defeat Proxima Midnight with Hard Challenge.
- Defeat Proxima Midnight with Heroic Challenge.
- Defeat Thanos.
- Defeat Thanos with Moderate Challenge.
- Defeat Thanos with Hard Challenge.
- Defeat Thanos with Heroic Challenge.

## RETURN OF THE SINISTER SIX:

- Defeat Doctor Octopus.
- Defeat Doctor Octopus with Moderate Challenge.
- Defeat Doctor Octopus with Hard Challenge.
- Defeat Doctor Octopus with Heroic Challenge.
- Defeat Electro.
- Defeat Electro with Moderate Challenge.
- Defeat Electro with Hard Challenge.
- Defeat Electro with Heroic Challenge.
- Defeat Kraven.
- Defeat Kraven with Moderate Challenge.
- Defeat Kraven with Hard Challenge.
- Defeat Kraven with Heroic Challenge.
- Defeat Mysterio.
- Defeat Mysterio with Moderate Challenge.
- Defeat Mysterio with Hard Challenge.
- Defeat Mysterio with Heroic Challenge.
- Defeat Sandman.
- Defeat Sandman with Moderate Challenge.
- Defeat Sandman with Hard Challenge.
- Defeat Sandman with Heroic Challenge.
- Defeat Vulture.
- Defeat Vulture with Moderate Challenge.
- Defeat Vulture with Hard Challenge.
- Defeat Vulture with Heroic Challenge.
- Defeat the Sinister Six.
- Defeat the Sinister Six with Moderate Challenge.
- Defeat the Sinister Six with Hard Challenge.
- Defeat the Sinister Six with Heroic Challenge.

## KICKSTARTER PROMOS:

- Defeat Baron Zemo.
- Defeat Baron Zemo with Moderate Challenge.
- Defeat Baron Zemo with Hard Challenge.
- Defeat Baron Zemo with Heroic Challenge.
- Defeat Bullseye.
- Defeat Bullseye with Moderate Challenge.
- Defeat Bullseye with Hard Challenge.
- Defeat Bullseye with Heroic Challenge.
- Defeat Carnage.
- Defeat Carnage with Moderate Challenge.
- Defeat Carnage with Hard Challenge.
- Defeat Carnage with Heroic Challenge.
- Defeat Corvus Glaive.
- Defeat Corvus Glaive with Moderate Challenge.
- Defeat Corvus Glaive with Hard Challenge.
- Defeat Corvus Glaive with Heroic Challenge.
- Defeat Dormammu.
- Defeat Dormammu with Moderate Challenge.
- Defeat Dormammu with Hard Challenge.
- Defeat Dormammu with Heroic Challenge.
- Defeat Hela.
- Defeat Hela with Moderate Challenge.
- Defeat Hela with Hard Challenge.
- Defeat Hela with Heroic Challenge.
- Defeat Kang.
- Defeat Kang with Moderate Challenge.
- Defeat Kang with Hard Challenge.
- Defeat Kang with Heroic Challenge
- Defeat Kingpin.
- Defeat Kingpin with Moderate Challenge.
- Defeat Kingpin with Hard Challenge.
- Defeat Kingpin with Heroic Challenge
- Defeat M.O.D.O.K.
- Defeat M.O.D.O.K. with Moderate Challenge.
- Defeat M.O.D.O.K. with Hard Challenge.
- Defeat M.O.D.O.K. with Heroic Challenge.
- Defeat Rhino.
- Defeat Rhino with Moderate Challenge.
- Defeat Rhino with Hard Challenge.
- Defeat Rhino with Heroic Challenge.
- Defeat Venom.
- Defeat Venom with Moderate Challenge.
- Defeat Venom with Hard Challenge.
- Defeat Venom with Heroic Challenge

# Hero Feats

## CORE BOX:

- Deal the last damage with Ant-Man.
- Play all Ant-Man’s Special Effect cards.
- Deal the last damage with Black Widow.
- Play all Black Widow’s Special Effect cards.
- Deal the last damage with Captain America.
- Play all Captain America’s Special Effect cards.
- Deal the last damage with Captain Marvel.
- Play all Captain Marvel’s Special Effect cards.
- Deal the last damage with Hulk.
- Play all Hulk’s Special Effect cards.
- Deal the last damage with Iron Man.
- Play all Iron Man’s Special Effect cards.
- Deal the last damage with Wasp.
- Play all Wasp’s Special Effect cards.

## RISE OF THE BLACK PANTHER:

- Deal the last damage with Black Panther.
- Play all Black Panther’s Special Effect cards.
- Deal the last damage with Shuri.
- Play all Shuri’s Special Effect cards.
- Deal the last damage with Winter Soldier.
- Play all Winter Soldier’s Special Effect cards.

## ENTER THE SPIDER-VERSE:

- Deal the last damage with Ghost-Spider.
- Play all Ghost-Spider’s Special Effect cards.
- Deal the last damage with Miles Morales.
- Play all Miles Morales’ Special Effect cards.
- Deal the last damage with Spider-Man.
- Play all Spider-Man’s Special Effect cards.

## TALES OF ASGARD:

- Deal the last damage with Korg.
- Play all Korg’s Special Effect cards.
- Deal the last damage with Thor.
- Play all Thor’s Special Effect cards.
- Deal the last damage with Valkyrie.
- Play all Valkyrie’s Special Effect cards.

## GUARDIANS OF THE GALAXY REMIX:

- Deal the last damage with Groot.
- Play all Groot’s Special Effect cards.
- Deal the last damage with Rocket.
- Play all Rocket’s Special Effect cards.
- Deal the last damage with Star-Lord.
- Play all Star-Lord’s Special Effect cards.

## KICKSTARTER PROMOS:

- Deal the last damage with Adam Warlock.
- Play all Adam Warlock’s Special Effect cards.
- Deal the last damage with America Chavez.
- Play all America Chavez’s Special Effect cards.
- Deal the last damage with Beta Ray Bill.
- Play all Beta Ray Bill’s Special Effect cards.
- Deal the last damage with Black Cat.
- Play all Black Cat’s Special Effect cards.
- Deal the last damage with Blade.
- Play all Blade’s Special Effect cards.
- Deal the last damage with Daredevil.
- Play all Daredevil’s Special Effect cards.
- Deal the last damage with Doctor Strange.
- Play all Doctor Strange’s Special Effect cards.
- Deal the last damage with Drax.
- Play all Drax’s Special Effect cards.
- Deal the last damage with Elektra.
- Play all Elektra’s Special Effect cards.
- Deal the last damage with Falcon.
- Play all Falcon’s Special Effect cards.
- Deal the last damage with Gamora.
- Play all Gamora’s Special Effect cards.
- Deal the last damage with Ghost Rider.
- Play all Ghost Rider’s Special Effect cards.
- Deal the last damage with Hawkeye.
- Play all Hawkeye’s Special Effect cards.
- Deal the last damage with Howard the Duck.
- Play all Howard the Duck’s Special Effect cards.
- Deal the last damage with Iron Fist.
- Play all Iron Fist’s Special Effect cards.
- Deal the last damage with Jessica Jones.
- Play all Jessica Jones’ Special Effect cards.
- Deal the last damage with Luke Cage.
- Play all Luke Cage’s Special Effect cards.
- Deal the last damage with Mantis.
- Play all Mantis’ Special Effect cards.
- Deal the last damage with Mockingbird.
- Play all Mockingbird’s Special Effect cards.
- Deal the last damage with Moon Knight.
- Play all Moon Knight’s Special Effect cards.
- Deal the last damage with Ms. Marvel.
- Play all Ms. Marvel’s Special Effect cards.
- Deal the last damage with Nebula.
- Play all Nebula’s Special Effect cards.
- Deal the last damage with Nick Fury.
- Play all Nick Fury’s Special Effect cards.
- Deal the last damage with Nova.
- Play all Nova’s Special Effect cards.
- Deal the last damage with Okoye.
- Play all Okoye’s Special Effect cards.
- Deal the last damage with Punisher
- Play all Punisher’s Special Effect cards.
- Deal the last damage with Quicksilver.
- Play all Quicksilver’s Special Effect cards.
- Deal the last damage with Scarlet Witch.
- Play all Scarlet Witch’s Special Effect cards.
- Deal the last damage with Shang Chi.
- Play all Shang Chi’s Special Effect cards.
- Deal the last damage with She-Hulk.
- Play all She-Hulk’s Special Effect cards.
- Deal the last damage with Spider-Ham.
- Play all Spider-Ham’s Special Effect cards.
- Deal the last damage with Spider-Man 2099.
- Play all Spider-Man 2099’s Special Effect cards.
- Deal the last damage with Spider-Woman.
- Play all Spider-Woman’s Special Effect card
- Deal the last damage with Squirrel Girl.
- Play all Squirrel Girl’s Special Effect cards.
- Deal the last damage with Venom.
- Play all Venom’s Special Effect cards.
- Deal the last damage with Vision.
- Play all Vision’s Special Effect cards.
- Deal the last damage with War Machine.
- Play all War Machine’s Special Effect cards.
- Deal the last damage with Yondu.
- Play all Yondu’s Special Effect cards.
