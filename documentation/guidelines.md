# Achievement types

## Hero feats

Feats specific to a single hero:

- Winning a game with a specific hero, at different difficulties
- Doing the action winning the game with a specific hero
- Using a combination of actions reminiscent of an iconic move/moment of a hero

## Villain feats

Feats specific to a single villain:

- Defeating a specific villain, at different difficulties
- Using a combination of actions reminiscent of an iconic move/moment against a single villain

## Campaign feats

Feats specific to a campaign

- Completing all steps of a campaign, at different difficulties
- Combination of conditions/actions reminiscent of an iconic move/moment in the story arc

## Team-ups

Feats involving winning with a specific combination of characters.

## Showdowns

Feats involving winning with a specific combination of characters against a specific villain
