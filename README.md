# Marvel Champions LCG Achievements

Marvel Champions LCG Achievements is a small project aiming at creating additionnal achievements to track your progress in the Marvel Champions LCG card game.

This README follows [MakeAREADME](https://www.makeareadme.com/) suggestions.

You can find the pdf document from the [latest release](https://gitlab.com/obrenckle/marvel-champions-lcg-achievements/-/releases/latest) here: <https://gitlab.com/api/v4/projects/51591146/packages/generic/output_pdfs/latest/MC_LCG_Achievements_latest.pdf>

## Contributing

Feel free to suggest new achievements by creating a new project issue.
